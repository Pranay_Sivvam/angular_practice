import { Component } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent {

  id: number;
  name: string;
  avg:number;

  address: any;
  hobbies: any;


  constructor(){

    this.id=101;
    this.name="Pranay";
    this.avg=33.33,
  
    this.address={
      streetNo: 101,
      city:'Hyderabad',
      state:'Telangana'
    }

    this.hobbies = ['Running', 'Walking', 'Swimming', 'Music', 'Cricket'];
  
  
  }
  


}
