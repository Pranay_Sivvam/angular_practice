import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test2',
  templateUrl: './test2.component.html',
  styleUrl: './test2.component.css'
})
export class Test2Component implements OnInit{

  person:any;
 
  constructor() {
    alert("Constructor Invoked...");
    this.person = {
      id: 101,
      name: 'Harsha',
      avg: 45.56,

      address: {
        streetNo: 101,
        city:'Hyderabad',
        state:'Telangana'
      },
      
      hobbies:['Running', 'Walking', 'Swimming', 'Music', 'Cricket']
    }

  }

  ngOnInit() {  
    alert("ngOnInit Invoked...");
  } 

 

}
